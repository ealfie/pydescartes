from __future__ import division
import numpy as np
from descartes import posibilidades_descartes


def posibilidades_a_priori(a):
    degree = len(a) - 1
    def filtra(p,n):
        return (p+n <= degree) and ( ((p+n) % 2) == (degree % 2) )

    comb = [ (p, n) for p in xrange(degree+1) for n in xrange(degree+1) if filtra(p,n) ]
    return comb



def factor_diezmado_descartes(a):
    num_a_priori = len(posibilidades_a_priori(a))
    num_descartes = len(posibilidades_descartes(a))
    factor = num_a_priori / num_descartes
    return factor


def random_poly(degree, poner_ceros=False):
    eps = np.finfo(np.double).eps
    K = 10.0
    a = np.zeros((degree+1,))
    for j in xrange(degree+1):
        sample = (2 * np.random.random_sample() - 1)
        if poner_ceros:
            if np.abs(sample) > 0.2 or j == 0:
                a[j] = K*sample
            else:
                a[j] = 0.0
        else:
            a[j] = K*sample
    return a


def estadistica_factor_diezmado(grado, N):
    l = list()
    for i in xrange(N):
        a = random_poly(grado)
        f = factor_diezmado_descartes(a)
        l.append(f)
    return l


def histograma_factor_diezmado(grado, N):
    muestras = estadistica_factor_diezmado(grado, N)
    H = dict()
    for f in muestras:
        if H.has_key(f):
            H[f] += 1
        else:
            H[f] = 1

    N=len(muestras)
    for k in H.keys():
        H[k] = float(H[k]) / N

    return H


