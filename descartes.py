from __future__ import division
import numpy as np


def _polinomio_conjugado(a):
    b = np.zeros_like(a)
    degree = len(a) - 1
    for j in xrange(len(a)):
        exponente = degree - j
        if exponente % 2 == 1:
            b[j] = -a[j]
        else:
            b[j] =  a[j]
    return b


def _num_sign_changes(a):
    nza = a[np.nonzero(a)]
    sign_changes = 0
    for j in xrange(1,len(nza)):
        if np.sign(nza[j]) != np.sign(nza[j-1]):
            sign_changes += 1
    return sign_changes


def _descartes_posibles(n):
    assert(n>=0)
    l = list()
    while n >= 0:
        l.append(n)
        n -= 2
    return l


def posibilidades_descartes(a):
    nmas   = _num_sign_changes(a)
    nmenos = _num_sign_changes(_polinomio_conjugado(a))

    pos_list = _descartes_posibles(nmas)
    neg_list = _descartes_posibles(nmenos)

    pos_list, neg_list = descartes_analysis(a)
    return [(p,n) for p in pos_list for n in neg_list]

